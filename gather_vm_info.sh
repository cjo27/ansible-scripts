   #!/bin/bash

   # Define output file
   OUTPUT_FILE="/tmp/serverinfo.info"

   # Collect date
   echo "Date:" > "$OUTPUT_FILE"
   date >> "$OUTPUT_FILE"

   # Collect the last 10 users who logged into the server
   echo -e "\nLast 10 logged in users:" >> "$OUTPUT_FILE"
   last -n 10 >> "$OUTPUT_FILE"

   # Collect swap space usage
   echo -e "\nSwap space usage:" >> "$OUTPUT_FILE"
   swapon --show >> "$OUTPUT_FILE" || echo "No swap configured" >> "$OUTPUT_FILE"

   # Collect kernel version
   echo -e "\nKernel version:" >> "$OUTPUT_FILE"
   uname -r >> "$OUTPUT_FILE"

   # Collect IP address
   echo -e "\nIP addresses:" >> "$OUTPUT_FILE"
   hostname -I >> "$OUTPUT_FILE"

   # Output completion message
   echo "VM information collected in $OUTPUT_FILE"
   

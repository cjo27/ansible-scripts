# Ansible Scripts

## Description
This repository contains an Ansible playbook for setting up a user and installing tmux on a server. And a bash script to gather the following info from server:
* date
* the last 10 users who logged to a server
* swap space
* kernel version
* ip address



## Files
- `co-adduser.yml`: Ansible playbook to create a user, expire their password, and install tmux.
- ` gather_vm_info.sh `: Bash script a bash script to gather the following info from server:
* date
* the last 10 users who logged to a server
* swap space
* kernel version
* ip address


## Usage
1. Clone the repository:
   
   git clone https://gitlab.com/cjo27/ansible-scripts.git
   cd ansible-scripts

